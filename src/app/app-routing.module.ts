import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatWindowComponent } from './components/chat-window/chat-window.component';
import { PuneBotComponent } from './components/pune-bot/pune-bot.component';


const routes: Routes = [
	
    // {
    //     path: ':location',
    //     component: ChatWindowComponent
    // },
    { path: '', component: ChatWindowComponent, pathMatch: 'full' },
    // {
    //     path: 'pune',
    //     component: PuneBotComponent
    // },
    { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
