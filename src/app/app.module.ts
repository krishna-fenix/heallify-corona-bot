import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChatWindowComponent } from './components/chat-window/chat-window.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BotServiceService } from './services/bot-service.service'
import { StoreValuesService } from './services/store-values.service';
import { SelectLanguageComponent } from './components/select-language/select-language/select-language.component'
import { MatDialogModule } from '@angular/material';
import { QaBoxComponent } from './components/qa-box/qa-box.component'
import { SanitizeHtmlPipe } from './sanitize-html.pipe';
import { AssessmentFormComponent } from './components/assessment-form/assessment-form.component';
import {
	MatDatepickerModule,
	MatNativeDateModule,
	MatFormFieldModule,
	MatInputModule,
	MatSelectModule,
	MatCardModule,
	MatTableModule,
	MatButtonModule,
	MatIconModule
  } from '@angular/material';
import { ReloadAssessmentComponent } from './components/reload-assessment/reload-assessment.component';
import { GetLocationComponent } from './components/get-location/get-location.component';
import { PuneBotComponent } from './components/pune-bot/pune-bot.component';

@NgModule({
	declarations: [
		AppComponent,
		ChatWindowComponent,
		SelectLanguageComponent,
		QaBoxComponent,
		SanitizeHtmlPipe,
		AssessmentFormComponent,
		ReloadAssessmentComponent,
		GetLocationComponent,
		PuneBotComponent
	],
	imports: [
		BrowserModule,
		RouterModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		MatIconModule,
		MatButtonModule,
		MatDialogModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatCardModule,
		MatTableModule
	],
	providers: [
		BotServiceService,
		StoreValuesService
	],
	exports: [
		SelectLanguageComponent,
		QaBoxComponent,
		AssessmentFormComponent,
		ReloadAssessmentComponent,
		GetLocationComponent
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
