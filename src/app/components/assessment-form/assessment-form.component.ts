import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import * as $ from 'jquery';
import { BotServiceService } from '../../services/bot-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CONFIG } from '../../../config';
@Component({
	selector: 'app-assessment-form',
	templateUrl: './assessment-form.component.html',
	styleUrls: ['./assessment-form.component.css']
})
export class AssessmentFormComponent implements OnInit {

	riskassessmentForm:FormGroup;

	constructor(
		private dialogRef:MatDialogRef<AssessmentFormComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private botService:BotServiceService
	) { }

	ngOnInit(): void {
		console.log(this.data);
		// this.initializeForm();
		this.riskassessmentForm = new FormGroup({
			name: new FormControl('', [Validators.required]),
			mobile:new FormControl('',[Validators.required]),
			email:new FormControl('',[Validators.email]),
			area:new FormControl(''),
			pincode:new FormControl(''),
			address: new FormControl(''),
		})
		// this.generateForm(this.data);
	}

	initializeForm(){
		var formObj = {};
		this.data.fields.forEach(field => {
			if(field == 'email')
				formObj[field] = new FormControl('', [Validators.required,Validators.email])
			else
				formObj[field] = new FormControl('', [Validators.required])
		});
		this.riskassessmentForm = new FormGroup(formObj);
	}

	generateForm(formData){
		var formDiv = $('<div class="risk-form"></div>');
		formData.fields.forEach(field => {
			var inputType = 'text';
			var input;
			if(field == 'email'){
				input = $(`<mat-form-field class="example-full-width">
				<mat-label>${field}</mat-label>
				<input type="email" matInput formControlName="${field}">
			  </mat-form-field>`);
			}else if(field == 'address'){
				input = $(`<mat-form-field class="example-full-width">
				<mat-label>${field}}</mat-label>
				<textarea matInput formControlName="${field}"></textarea>
			  </mat-form-field>`);
			}else{
				input = $(`<mat-form-field class="example-full-width">
				<mat-label>${field}</mat-label>
				<input matInput formControlName="${field}">
			  </mat-form-field>`);
			}
			formDiv.append(input);
		});
		formDiv.append('<button type="submit" class="action checkbox-submission">Submit</button>');
		$('#r-a-form').append(formDiv);
	}

	onFormSubmit(value){
		console.log(value);

		var api_url = CONFIG.API_URL + this.data.api.URL;
		var method = this.data.api.method;
		var parameters = this.data.api.parameters;
		var json = {
			parameters : parameters,
			form_data: value
		}
		this.botService.submitRiskAssessmentForm(api_url, method, json).subscribe(
			response => {
				console.log(response);
				this.onClose();
			},
			error => {
				console.log(error);
			}
		)
	}

	isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}

	onClose() {
		this.dialogRef.close();
	}


}
