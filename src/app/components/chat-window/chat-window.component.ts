import { Component, OnInit, AfterViewChecked, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { BotServiceService } from '../../services/bot-service.service';
import * as $ from 'jquery';
import { CONFIG } from '../../../../src/config';
import {} from 'googlemaps';
import { MatDialog } from '@angular/material';
import { SelectLanguageComponent } from '../select-language/select-language/select-language.component';
import { QaBoxComponent } from '../../components/qa-box/qa-box.component';
import { AssessmentFormComponent } from '../../components/assessment-form/assessment-form.component';
import { ReloadAssessmentComponent } from '../reload-assessment/reload-assessment.component';
import { GetLocationComponent } from '../get-location/get-location.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-chat-window',
	templateUrl: './chat-window.component.html',
	styleUrls: ['./chat-window.component.css']
})
export class ChatWindowComponent implements OnInit {

	apiResponse:any;
	sessionId:any;
	userResponse = '';
	toggleButton:boolean = false;
	count = 1;
	storeURL: any;
	userCount = 1;
	today = new Date();
	amOrPm = (this.today.getHours() < 12) ? "AM" : "PM";
	time = this.today.getHours() + ":" + this.today.getMinutes() + ' ' + this.amOrPm;
	ageSubmission:boolean = false;
	typeCount = 0;
	apiCall = 0;
	checkBoxResponse:string[] = [];
	hideBubble:boolean = false;
	hideContinue:boolean = false;
	multilingualText:any;
	selectedLanguage:string = 'english';
	start_disclaimer:string = '';
	welcome_msg:string = '';
	start_btn:string = '';
	restart_assessment:string = '';
	locationData:any;
	hideStartText:boolean = false;
	risk_assessment:any;
	@ViewChild('scrollMe') private myScrollContainer: ElementRef;
	@ViewChild('map') mapElement: any;
	map: google.maps.Map;
	assessmentMessage:any = [];
	national_number:string = '';
	state_number:string = '';
	download_link_msg:string = '';
	city_wise:boolean = false;
	city:string = 'pune';
	mobileView:boolean = false;
	feedback:boolean = false;
	constructor(
		private botService:BotServiceService,
		public dialog: MatDialog,
		private route:ActivatedRoute,
		private router:Router,
	) { }


	ngOnInit() {

		// unomment below code after testing location
		this.getLocation();
		this.sessionId = new Date().getTime();
		this.readLanguageData();
		this.openSelectLanguageBox();
		// this.fadeOutOnMobile();
		// Comment below code after testing location
		// var mapCords = prompt("Please enter latitude & longitude (ex. 18.5492891,73.9474055)", "18.5492891,73.9474055");
		// if (mapCords != null) {
		// 	console.log(mapCords);
		// 	var cords = mapCords.split(',')
		// 	this.testLocation(cords[0],cords[1]);
		// }
			
	}

	ngAfterViewInit() {
		this.scrollToBottom();
	}

	ngAfterViewChecked() {
		this.scrollToBottom();
	}

	reload(){
		window.location.reload();
	}

	testLocation(lat, long){
		this.botService.getCurentLocation(lat,long).subscribe(
			response => {
				console.log(response);
				this.locationData = response.results[0].address_components;
			},
			error => {
				console.log(error);
			}
		)
	}

	isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}

	// fadeOutOnMobile(){
	// 	if($(window).innerWidth() <= 425) {
	// 		setTimeout(() => {
				
	// 		}, timeout);
	// 	}
	// }

	readLanguageData(){
		this.botService.getLanguageData().subscribe(
			data => {
				this.multilingualText = data;
				this.start_disclaimer = this.multilingualText['english'].disclaimer;
				this.start_btn = this.multilingualText['english'].continue;
				this.welcome_msg = this.multilingualText['english'].welcome;
				this.restart_assessment = this.multilingualText['english']['restart assessment'];
				this.national_number = this.multilingualText['english'].national_number;
				this.state_number = this.multilingualText['english'].state_number;
				this.download_link_msg = this.multilingualText['english'].download_link_msg;
				console.log(this.multilingualText);
			},	
			error => {
				console.log(error);
			}
		)
	}

	openSelectLanguageBox(): void {
		const dialogRef = this.dialog.open(SelectLanguageComponent, {
			width: '700px',
			height: '340px',
			disableClose: true
		});
		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed', result);
			this.selectedLanguage = result;
			if(result != 'english' && result != undefined){
				this.start_disclaimer = this.multilingualText[result].disclaimer + ' / ' + this.multilingualText['english'].disclaimer;
				this.start_btn = this.multilingualText[result].continue + ' / ' + this.multilingualText['english'].continue;
				this.welcome_msg = this.multilingualText[result].welcome + ' / ' + this.multilingualText['english'].welcome;
				this.restart_assessment = this.multilingualText[result]['restart assessment'] + ' / ' + this.multilingualText['english']['restart assessment'];
				this.national_number = this.multilingualText[result].national_number;
				this.state_number = this.multilingualText[result].state_number;
				this.download_link_msg = this.multilingualText[result].download_link_msg;
			}
			setTimeout(() => {
				this.hideBubble = true;
				this.hideStartText = true;
			},700);
			if($(window).innerWidth() <= 425) {
				this.mobileView = true;
				$('#myTopnav').addClass('animated fadeOutUp')
				setTimeout(() => {
					$('#myTopnav').css('display','none');
				}, 700);
			}

		});
	}

	openQABox(qaAnswer): void {
		const dialogRef = this.dialog.open(QaBoxComponent, {
			width: '500px',
			height: 'fit-content',
			data:qaAnswer
		});
		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed', result);
		});
	}

	openAssessmentForm(): void {
		const dialogRef = this.dialog.open(AssessmentFormComponent, {
			width: '500px',
			height: 'fit-content',
			data:this.risk_assessment.formPopUp
		});
		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed', result);
		});
	}

	askForRestart(): void {
		const dialogRef = this.dialog.open(ReloadAssessmentComponent, {
			width: '500px',
			height: 'fit-content'
		});
	}

	askLocationFromUser(): void {
		const dialogRef = this.dialog.open(GetLocationComponent, {
			width: '500px',
			height: 'fit-content'
		});
		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed', result);
			if(result)
				this.locationData = result;
		});
	}


	// enter presss key function
	keyDownFunction(event) {
		if (event.keyCode === 13) {
			this.onClickSend()
		}
	}

	getLocation() {
		if (navigator.geolocation) {
		  	navigator.geolocation.getCurrentPosition((position)=> {
		  		console.log(position);
				this.botService.getCurentLocation(position.coords.latitude,position.coords.longitude).subscribe(
					response => {
						var location = response.results[0].address_components;
						this.locationData = {};
						this.locationData['lat'] = position.coords.latitude;
						this.locationData['long'] = position.coords.longitude;
						location.forEach(component => {
							if(component.types.indexOf("administrative_area_level_1") != -1)
								this.locationData['state'] = component.long_name;
							else if(component.types.indexOf("administrative_area_level_2") != -1)
								this.locationData['city'] = component.long_name;
							else if(component.types.indexOf("country") != -1){
								this.locationData['country'] = component.long_name;
							}
						});
						// this.locationData = response.results[0].address_components;
					},
					error => {
						console.log(error);
					}
				)
			  },(error) => {
					console.log(error);
					console.log("User denied/block to access the geolocation!!!");
					// if(error.code){
					// 	this.askLocationFromUser();
					// }
			  });
		} else { 
		  	alert("Geolocation is not supported by this browser.");
		}
	}

	scrollToBottom(): void {
		try {
			this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
		} catch (err) { }
	}

	onClickSend() {
		var formatedText = '';
		if(Array.isArray(this.checkBoxResponse) && this.checkBoxResponse.length > 0){
			var arrResponse = []
			this.checkBoxResponse.forEach(element => {
				var responseText = (this.selectedLanguage != 'english' ? ( this.multilingualText[this.selectedLanguage][element] ? this.multilingualText[this.selectedLanguage][element] + ' / ' + this.multilingualText['english'][element] : element) : element);
				arrResponse.push(responseText);
			})
			formatedText = arrResponse.join();

		}else{
			if(!this.ageSubmission)
				formatedText = (this.selectedLanguage != 'english' ? ( this.multilingualText[this.selectedLanguage][this.userResponse] ? this.multilingualText[this.selectedLanguage][this.userResponse] + ' / ' + this.multilingualText['english'][this.userResponse] : this.multilingualText['english'][this.userResponse]) : this.userResponse)
			else{
				formatedText = (this.selectedLanguage != 'english' ? ( this.multilingualText[this.selectedLanguage]['years'] ? this.userResponse + ' / ' + this.multilingualText[this.selectedLanguage]['years'] : this.userResponse) : this.userResponse);
			}
		}

		let enterdata = $('<div class="message-container right">').append($('<div class="response-wrap" id="response-wrap">')
				.append($('<div class="response" id="response">')
				.append(
					$('<div class="text" id="userTextSend">').append($('<p style="margin:0;">').text(formatedText)).append($('<span class="mat-time">'+this.time+' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'))
				)
			)
		);
		enterdata.attr('id', 'right' + this.userCount);

		$('.append').after(enterdata);
		$('.append').remove();
		$('<div class="append">').insertAfter($('#right' + this.userCount));
		this.userCount = this.userCount + 1;
		console.log(this.userResponse);
		this.botService.getResponse(this.sessionId, this.userResponse).subscribe((data) => {
			this.userResponse = '';
			this.ageSubmission = false;
			this.hideBubble = true;
			this.checkBoxResponse = [];
			this.generateHTML(data);
		});
	}

	welcomeFunction() {
		var text = (this.selectedLanguage != 'english' ? (this.multilingualText[this.selectedLanguage].continue ? this.multilingualText[this.selectedLanguage].continue + " / Continue" : "Continue") : "Continue");
		this.hideContinue = true;
		this.hideBubble = false;
		var that = this;
		var today = new Date();
		var amOrPm = (today.getHours() < 12) ? "AM" : "PM";
		var time = today.getHours() + ":" + today.getMinutes() + ' ' + amOrPm;
		const enterdata = $('<div class="message-container right">').append($('<div class="response-wrap" id="response-wrap">')
				.append($('<div class="response" id="response">')
					.append($('<div class="text" id="userTextSend">').append($('<p style="margin:0;">').text(text)).append($('<span class="mat-time">'+time+' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'))
					)
				)
			);
		enterdata.attr('id', 'right0');

		$('.append').after(enterdata);
		$('.append').remove();
		$('<div class="append">').insertAfter($('#right0'));
		var contextData = {
			"name": "getLocation",
			"parameters":{
				"city": 'Pune',
				"state": 'Maharashtra',
				"country": 'India'
			}
	
		}
		if(this.locationData){
			contextData["parameters"] = this.locationData;
		}
		console.log('click button ' + text);
		that.botService.getFirstResponse(that.sessionId, 'continue', contextData).subscribe((data) => {
			// this.userResponse = '';
			that.hideBubble = true;
			that.generateHTML(data);
		});
	}

	generateWelcomeMessgae(welcomeResponse) {
		const welcomedata = $('<div class="message-container" id="welcome-container">').append($('<div class="message type-1 passed">')
			.append($('<div class="avatar-container" id="avatar">')));

		$('#message-box').append(welcomedata);
		$('<div class="append">').insertAfter($('#welcome-container'));
		const responseDiv = document.getElementById('firstavatar');
		for (let i = 0; i < welcomeResponse.data.text.length; i++) {
			console.log('in loop ' + welcomeResponse.data.text[i]);
			responseDiv.innerHTML += '<div class="question" id="question"> <div class="text">' + welcomeResponse.data.text[i] + '</div> </div>';
		}
	}

	generateHTML(response) {
		var isTextPrinted = false;
		var skip = false;
		var skip_btn;
		// this.hideBubble = false;
		// if(response.data.hasOwnProperty("skip")){
		// 	var that = this;
		// 	if(response.data.skip){
		// 		skip_btn = $('<div><button type="button" id="skip-it-'+this.count+'" class="skip_action">Skip</button></div>');
		// 		$(document).on('click', '#skip-it-'+ this.count, function () {
		// 			that.skipQuestion(that);
		// 		});
		// 	}
		// }

		if (response.data.text && !response.data.buttons && Object.keys(response.data.payload).length === 0) {
			console.log('in text');
			this.toggleButton = true;
			var userDataDiv = $('<div class="avatar-container" id="avatar">');
			for (let i = 0; i < response.data.text.length; i++) {
				var responseText = response.data.text[i].toLowerCase();
				var formatLang = ((this.selectedLanguage != 'english') ? ( (this.multilingualText[this.selectedLanguage][responseText]) ? this.multilingualText[this.selectedLanguage][responseText] + ' / ' + this.multilingualText['english'][responseText] : response.data.text[i]) : response.data.text[i]);
				userDataDiv.append(
					$('<div class="question animated fadeInLeft" id="que">')
					.append(
						$('<div class="text">').append($('<p style="margin:0;">').text(formatLang)).append($('<span class="mat-time">'+this.time+' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'))
					)
				);
			}
			const userText = document.querySelector('#userRes');
			const htmldata = $('<div class="message-container">').append($('<div class="message type-1 passed">').append(userDataDiv).append(skip_btn));
			htmldata.attr('id', 'message-container' + this.count);
			$('.append').after(htmldata);
			$('.append').remove();
			$('<div class="append">').insertAfter($('#message-container' + this.count));
			$('#message-box').append($('#message-container'));
			this.count = this.count + 1;
			console.log('con inc' + this.count);
			console.log('acc response' + JSON.stringify(response));
			isTextPrinted = true;
		}

		if (response.data.text && response.data.buttons && Object.keys(response.data.payload).length === 0) {	
			console.log('in text with button');
			this.toggleButton = false;
			var check = this.apiCall;
			var userDataDiv = $('<div class="avatar-container" id="avatar">');
			for (let i = 0; i < response.data.text.length; i++) {
				var responseText = response.data.text[i].toLowerCase();
				var formatLang = ((this.selectedLanguage != 'english') ? ( (this.multilingualText[this.selectedLanguage][responseText]) ? this.multilingualText[this.selectedLanguage][responseText] + ' / ' + this.multilingualText['english'][responseText] : response.data.text[i]) : response.data.text[i]);
				userDataDiv.append(
					$('<div class="question animated fadeInLeft" id="que">')
					.append(
						$('<div class="text">').append($('<p style="margin:0;">').text(formatLang)).append($('<span class="mat-time">'+this.time+' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'))
					)
				);
			}
		
			var userButton = $('<div class="actions-container animated fadeInRight delay-1s" id="actionContainer-btn-'+check+'">');
			var demovar = '';
			var buttonClickNew = this.buttonClick;
			var that = this;
			for (let i = 0; i < response.data.buttons.length; i++) {
				console.log('in loop ' + response.data.buttons[i]);
				var responseText = response.data.buttons[i].toLowerCase();
				var formatLang = ((this.selectedLanguage != 'english') ? ( (this.multilingualText[this.selectedLanguage][responseText]) ? this.multilingualText[this.selectedLanguage][responseText] + ' / ' + this.multilingualText['english'][responseText] : response.data.buttons[i]) : response.data.buttons[i]);
				userButton.append($(' <div class="wrap">')
					.append($('<div tabindex="1" class="action action-btn" id="actionButton' + i + '-' + this.count + '" >').append($('<span>').text(formatLang))));

				demovar = response.data.buttons[i];
				$(document).on('click', '#actionButton' + i + '-' + this.count, function () {
					that.hideBubble = false;
					demovar = response.data.buttons[i].toLowerCase();
					$('#actionContainer-btn-'+check).hide();
					buttonClickNew(demovar, that);
					console.log(demovar);
				});

			}
		
			const userText = document.querySelector('#userRes');
			const htmldata = $('<div class="message-container">').append($('<div class="message type-1 passed">').append(userDataDiv)
				.append(userButton).append(skip_btn));
			htmldata.attr('id', 'message-container' + this.count);
			$('.append').after(htmldata);
			$('.append').remove();
			$('<div class="append">').insertAfter($('#message-container' + this.count));
			$('#message-box').append($('#message-container'));

			this.count = this.count + 1;
			console.log('con inc' + this.count);

			console.log('acc response' + JSON.stringify(response));
			isTextPrinted = true;

		}

		if(response.data.text && response.data.payload && Object.keys(response.data.payload).length > 0){
			// this.toggleButton = true;
			var qaDiv;
			var flexDirection = 'row'
			var that = this;
			
			var userDataDiv = $('<div class="avatar-container" id="avatar">');
			for (let i = 0; i < response.data.text.length; i++) {
				var responseText = response.data.text[i].toLowerCase();
				var formatLang = ((this.selectedLanguage != 'english') ? ( (this.multilingualText[this.selectedLanguage][responseText]) ? this.multilingualText[this.selectedLanguage][responseText] + ' / ' + this.multilingualText['english'][responseText] : response.data.text[i]) : response.data.text[i]);
				if(response.data.payload.qa){
					var qaAnswer = response.data.payload.qa.Answer;
					var qaformatLang = ((this.selectedLanguage != 'english') ? ( (this.multilingualText[this.selectedLanguage][response.data.payload.qa.Question]) ? this.multilingualText[this.selectedLanguage][response.data.payload.qa.Question] + ' / ' + this.multilingualText['english'][response.data.payload.qa.Question] : response.data.payload.qa.Question) : response.data.payload.qa.Question);
					flexDirection = 'column';
					qaDiv = $('<div><span class="qa-action" id="qa-action-'+this.apiCall+'">'+qaformatLang+'</span></div>');
					$(document).on('click', '#qa-action-'+this.apiCall, function(){
						that.openQABox(qaAnswer);
					})
				}
				userDataDiv.append(
					$('<div class="question animated fadeInLeft" id="que" style="flex-direction:'+flexDirection+'">')
					.append(
						$('<div class="text">').append($('<p style="margin:0;">').text(formatLang)).append($('<span class="mat-time">'+this.time+' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'))
					).append(qaDiv)
				);
			}

			if(response.data.payload.hasOwnProperty("checkBox")){
				var userCheckBox = $('<div class="actions-container animated fadeInRight delay-1s" id="actionContainer-checboxes-'+this.apiCall+'">');
				var wrap = $('<div class="wrap">');
				var actions = $('<div tabindex="1" class="expect-salary-action" id="actionButton-' + this.count + '" >');
				var that = this;
				var check = this.apiCall;
				var isOtherPresent = false;
				var submitFormattedText = ((this.selectedLanguage != 'english') ? ( (this.multilingualText[this.selectedLanguage]["none of these"]) ? this.multilingualText[this.selectedLanguage]["none of these"] + ' / ' + this.multilingualText['english']["none of these"] : "None of These") : "None of These");
				for (let i = 0; i < response.data.payload.checkBox.length; i++) {
					console.log('in loop ' + response.data.payload.checkBox[i]);
					var responseText = response.data.payload.checkBox[i].toLowerCase();
					var formatLang = ((this.selectedLanguage != 'english') ? ( (this.multilingualText[this.selectedLanguage][responseText]) ? this.multilingualText[this.selectedLanguage][responseText] + ' / ' + this.multilingualText['english'][responseText] : response.data.payload.checkBox[i]) : response.data.payload.checkBox[i]);
				
					actions.append($('<div class="checkBoxContainer" style="display:flex;"><label class="label-container">'+formatLang+'<input type="checkbox" class="ex-chckbox-input" id="checkBoxButton' + i + '-' + this.count + '" value="'+response.data.payload.checkBox[i]+'"><span class="checkmark"></span>'));
					
					$(document).on('click', '#checkBoxButton' + i + '-' + this.count, function (event) {
						
						if($(this).is(":checked")){
							console.log(this.value.toLowerCase());
							console.log(that.checkBoxResponse);
							if(isOtherPresent && this.value == "Other"){
								$('#other-country').show();
								that.checkBoxResponse.push('othercountry');
							}else{
								that.checkBoxResponse.push(this.value.toLowerCase());
							}
							submitFormattedText = ((that.selectedLanguage != 'english') ? ( (that.multilingualText[that.selectedLanguage]["submit"]) ? that.multilingualText[that.selectedLanguage]["submit"] + ' / ' + that.multilingualText['english']["submit"] : "Submit") : "Submit");
							document.getElementById('checkbox-submission-none-'+check).style.display = 'none';
							document.getElementById('checkbox-submission-'+check).style.display = 'flex';
							document.getElementById('checkbox-submission-'+check).innerText = submitFormattedText;
						}else{
							if(isOtherPresent && this.value == "Other")
								$('#other-country').hide();
							that.checkBoxResponse.splice(that.checkBoxResponse.indexOf(this.value), 1);
							console.log(that.checkBoxResponse)
							if(that.checkBoxResponse.length == 0){
								submitFormattedText = ((that.selectedLanguage != 'english') ? ( (that.multilingualText[that.selectedLanguage]["none of these"]) ? that.multilingualText[that.selectedLanguage]["none of these"] + ' / ' + that.multilingualText['english']["none of these"] : "None of These") : "None of These");
								document.getElementById('checkbox-submission-none-'+check).style.display = 'flex';
								document.getElementById('checkbox-submission-none-'+check).innerText = submitFormattedText;
								document.getElementById('checkbox-submission-'+check).style.display = 'none';
							}
						}
						
					});
					this.count = this.count + 1;
				}
				if(response.data.payload.hasOwnProperty("ifOtherEnterValue") && response.data.payload.ifOtherEnterValue){
					isOtherPresent = true;
					actions.append($('<input type="text" placeholder="Enter Other Country" class="age-input" id="other-country" style="display:none;">'))
				}
				wrap.append(actions);
				userCheckBox.append(wrap).append($('<button type="button" class="action checkbox-submission" id="checkbox-submission-none-'+this.apiCall+'">'+submitFormattedText+'</button> <button type="button" style="display:none;" class="action checkbox-submission chbx-d-none" id="checkbox-submission-'+this.apiCall+'">'+ submitFormattedText + '</button>'));
				
				$(document).on('keypress', '#other-country', function (event) {
					if(event.keyCode == 13){
						that.hideBubble = false;
						if(that.checkBoxResponse.indexOf('othercountry') != -1){
							var otherCountryVal = $('#other-country').val();
							console.log(otherCountryVal);
							if(otherCountryVal == '')
								otherCountryVal = "none";
							that.checkBoxResponse.splice(that.checkBoxResponse.indexOf('othercountry'),1);
							that.checkBoxResponse.push(otherCountryVal); 
							$('#actionContainer-checboxes-'+check).hide();
							that.userResponse = that.checkBoxResponse.join(",");
							that.onClickSend();
						}
					}
				});
				
				$(document).on('click', '#checkbox-submission-'+ this.apiCall, function () {
					if(that.checkBoxResponse.indexOf('othercountry') != -1){
						var otherCountryVal = $('#other-country').val();
						console.log(otherCountryVal);
						if(otherCountryVal == '')
							otherCountryVal = "none";
						that.checkBoxResponse.splice(that.checkBoxResponse.indexOf('othercountry'),1);
						that.checkBoxResponse.push(otherCountryVal); 
					}
					that.hideBubble = false;
					that.userResponse = that.checkBoxResponse.join(",");
					$('#actionContainer-checboxes-'+check).hide();
					that.onClickSend();
				});
				$(document).on('click', '#checkbox-submission-none-'+this.apiCall, function () {
					that.userResponse = 'none';
					that.hideBubble = false;
					$('#actionContainer-checboxes-'+check).hide();
					that.onClickSend();
				});
				// this.hideBubble = true;
				const htmldata = $('<div class="message-container">').append($('<div class="message type-1 passed">').append(userDataDiv)
				.append(userCheckBox).append(skip_btn));
				htmldata.attr('id', 'message-container' + this.count);
				$('.append').after(htmldata);
				$('.append').remove();
				$('<div class="append">').insertAfter($('#message-container' + this.count));
				$('#message-box').append($('#message-container'));

				this.count = this.count + 1;
	
			}

			else if(response.data.payload.hasOwnProperty("display")){
				
				var userTextBox = $('<div class="actions-container animated fadeInRight delay-1s" id="actionContainer-age">');
				var that = this;
				var submitFormattedText = ((that.selectedLanguage != 'english') ? ( (that.multilingualText[that.selectedLanguage]["submit"]) ? that.multilingualText[that.selectedLanguage]["submit"] + ' / ' + that.multilingualText['english']["submit"] : "Submit") : "Submit");
				userTextBox.append($(' <div class="wrap">')
				.append($('<div tabindex="1" class="expect-salary-action" id="actionButton-' + this.count + '" >')
				.append(
						$(
						'<input type="text" placeholder="Enter Age Here" class="age-input" id="inputButton-' + this.count + '"><p class="agr-error">Age can\'t be empty or zero!</p><button type="button" class="action" id="age-submission">'+ submitFormattedText + '</button>'
						)
					)
					)
					);

				$(document).on('input','#inputButton-' + this.count, function(){
					if(this.value < 121)
						return true;
					else{
						var str = this.value;
						str = str.substring(0, str.length - 1);
						this.value = str;
					}
				})
				
				$(document).on('keypress', '#inputButton-' + this.count, function (event) {
					$(this).css('border-bottom',"2px solid #743BD6");
					$('.agr-error').hide();
					if(event.keyCode == 13){
						console.log("value from salary input")
						console.log(event);
						if(this.value == 0){
							$(this).val('');
							$(this).css('border-bottom',"2px solid #e3513f");
							$('.agr-error').show();
							// alert("Age can't be zero!!!");
						}else{
							that.hideBubble = false;
							that.ageSubmission = true;
							that.userResponse = this.value + ' ' + response.data.payload.display.after;
							that.onClickSend()
							$('#actionContainer-age').hide();
						}
					}
					else{
						var charCode = (event.which) ? event.which : event.keyCode;
						if (charCode > 31 && (charCode < 48 || charCode > 57))
							return false;
						return true;
					}
				});

				$(document).on('click', '#age-submission', function () {
					this.value = $(this).prev().prev().val();
					if(this.value == 0 || this.value == ''){
						$(this).value = '';
						$(this).prev().prev().css('border-bottom',"2px solid #e3513f");
						$('.agr-error').show();
					}else{
						that.hideBubble = false;
						that.ageSubmission = true;
						that.userResponse =  this.value + ' ' + response.data.payload.display.after;
						that.onClickSend()
						$('#actionContainer-age').hide();
					}
				});

				const htmldata = $('<div class="message-container">').append($('<div class="message type-1 passed">').append(userDataDiv)
				.append(userTextBox).append(skip_btn));
				htmldata.attr('id', 'message-container' + this.count);
				$('.append').after(htmldata);
				$('.append').remove();
				$('<div class="append">').insertAfter($('#message-container' + this.count));
				$('#message-box').append($('#message-container'));

				this.count = this.count + 1;
			}

			else if (response.data.payload.hasOwnProperty("qa")) {	
				console.log('in text with button');
				this.toggleButton = false;
				var check = this.apiCall;
				
				var userButton = $('<div class="actions-container animated fadeInRight delay-1s" id="actionContainer-btn-'+check+'">');
				var demovar = '';
				var buttonClickNew = this.buttonClick;
				var that = this;
				for (let i = 0; i < response.data.buttons.length; i++) {
					console.log('in loop ' + response.data.buttons[i]);
					var responseText = response.data.buttons[i].toLowerCase();
					var formatLang = ((this.selectedLanguage != 'english') ? ( (this.multilingualText[this.selectedLanguage][responseText]) ? this.multilingualText[this.selectedLanguage][responseText] + ' / ' + this.multilingualText['english'][responseText] : response.data.buttons[i]) : response.data.buttons[i]);
					userButton.append($(' <div class="wrap">')
						.append($('<div tabindex="1" class="action action-btn" id="actionButton' + i + '-' + this.count + '" >').append($('<span>').text(formatLang))));
	
					demovar = response.data.buttons[i];
					$(document).on('click', '#actionButton' + i + '-' + this.count, function () {
						that.hideBubble = false;
						demovar = response.data.buttons[i].toLowerCase();
						$('#actionContainer-btn-'+check).hide();
						buttonClickNew(demovar, that);
						console.log(demovar);
					});
	
				}
		
				// this.hideBubble = true;
				const htmldata = $('<div class="message-container">').append($('<div class="message type-1 passed">').append(userDataDiv)
					.append(userButton).append(skip_btn));
				htmldata.attr('id', 'message-container' + this.count);
				$('.append').after(htmldata);
				$('.append').remove();
				$('<div class="append">').insertAfter($('#message-container' + this.count));
				$('#message-box').append($('#message-container'));
	
				this.count = this.count + 1;
				console.log('con inc' + this.count);
	
				console.log('acc response' + JSON.stringify(response));
	
			}

			else if(response.data.payload.hasOwnProperty("riskLevel")){
				// this.hideBubble = true;
				this.risk_assessment = response.data.payload;
				// $('#risk-assessment').show();
				// var that = this;
				setTimeout(() => {
					$('html, body').animate({
						scrollTop: $("#risk-assessment").offset().top
					}, 500);
				},500);
				
				this.risk_assessment.additionalMessages.forEach(msg => {
					var formattedText = (this.selectedLanguage != 'english' ? (this.multilingualText[this.selectedLanguage][msg] ? this.multilingualText[this.selectedLanguage][msg] : msg) : msg);	
					this.assessmentMessage.push(formattedText);
				});
				$('#message-box').css('border-bottom','2px solid #743BD6');
				$("#myTopnav").show()
				$('#myTopnav').removeClass('fadeOutUp').addClass('fadeInDown');
				// if(this.risk_assessment.hasOwnProperty('formPopUp')){
				// 	var data = {
				// 		parameters: this.risk_assessment.formPopUp.api.parameters
				// 	}
				// 	this.botService.submitRiskAssessmentForm("https://cyvh1io2qa.execute-api.ap-south-1.amazonaws.com/v1" + this.risk_assessment.formPopUp.api.URL, this.risk_assessment.formPopUp.api.method, data).subscribe(
				// 		response => {
				// 			console.log(response);
				// 		},
				// 		error => {
				// 			console.log(error);
				// 		}
				// 	)
				// }
				
				
				// 	this.openAssessmentForm();
				
			}

			// else{
			// 	if(!isTextPrinted){
			// 		this.hideBubble = true;
			// 		const htmldata = $('<div class="message-container">').append($('<div class="message type-1 passed">').append(userDataDiv).append(skip_btn));
			// 		htmldata.attr('id', 'message-container' + this.count);
			// 		$('.append').after(htmldata);
			// 		$('.append').remove();
			// 		$('<div class="append">').insertAfter($('#message-container' + this.count));
			// 		$('#message-box').append($('#message-container'));
			// 		this.count = this.count + 1;
			// 	}
			// }

		}
		this.apiCall++;
	}

	buttonClick(text, that) {
		console.log('in fubn');
		that.hideBubble = false;
		var response = text;
		text = (that.selectedLanguage != 'english' ? (that.multilingualText[that.selectedLanguage][text] ? that.multilingualText[that.selectedLanguage][text] + ' / ' + that.multilingualText['english'][text] : text) : text);
		var today = new Date();
		var amOrPm = (today.getHours() < 12) ? "AM" : "PM";
		var time = today.getHours() + ":" + today.getMinutes() + ' ' + amOrPm;
		const enterdata = $('<div class="message-container right">').append($('<div class="response-wrap" id="response-wrap">')
				.append($('<div class="response" id="response">')
					.append($('<div class="text" id="userTextSend">').append($('<p style="margin:0;">').text(text)).append($('<span class="mat-time">'+time+' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'))
					)
				)
			);
		enterdata.attr('id', 'right' + that.userCount);

		$('.append').after(enterdata);
		$('.append').remove();
		$('<div class="append">').insertAfter($('#right' + that.userCount));
		that.userCount = that.userCount + 1;
		// this.buttonClick(response.data.buttons[i]);
		// var userResponseNew = response.data.buttons[i];
		console.log('click button ' + text);
		that.botService.getResponse(that.sessionId, response).subscribe((data) => {
			// this.userResponse = '';
			that.hideBubble = true;
			that.generateHTML(data);
		});
	}

	skipQuestion(that){
		that.botService.getResponse(that.sessionId, 'skip').subscribe((data) => {
			that.generateHTML(data);
		});
	}

	isAssessmentHelpful(value){
		var parameters = {
			helpful:value
		}
		this.botService.isAssessmentHelpful(parameters).subscribe(
			response => {
				console.log(response);
				this.feedback = true;
			},
			error => {
				console.log(error);
			}
		)
	}


}
