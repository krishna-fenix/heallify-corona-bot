import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import * as $ from 'jquery';
import { BotServiceService } from '../../services/bot-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
	selector: 'app-get-location',
	templateUrl: './get-location.component.html',
	styleUrls: ['./get-location.component.css']
})
export class GetLocationComponent implements OnInit {

	getLocationForm:FormGroup;
	countryArray: any = [];
	stateArray: any = [];
	citiesArray: any = [];

	constructor(
		private dialogRef:MatDialogRef<GetLocationComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private botService:BotServiceService
	) { }

	ngOnInit(): void {
		// this.getCountries();
		this.getLocationForm = new FormGroup({
			city: new FormControl('', [Validators.required]),
			state:new FormControl('',[Validators.required]),
			country:new FormControl('',[Validators.required])
		})
	}

	getCountries(){
		this.botService.getCountries().subscribe(
			(response) => {
				console.log(response);
				response.forEach((item) => {
					this.countryArray.push(item.country_name);
				});
				console.log(this.countryArray);
			},
			(error) => {
				console.log(error);
			}
		)
	}

	getStates(country_name){
		this.stateArray = [];
		this.botService.getStates(country_name).subscribe(
			(response) => {
				response.forEach((item) => {
					this.stateArray.push(item.state_name);
				});
			},
			(error) => {
				console.log(error);
			}
		)
	}

	getCities(state_name){
		this.citiesArray = [];
		this.botService.getCities(state_name).subscribe(
			(response) => {
				response.forEach((item) => {
					this.citiesArray.push(item.city_name);
				});
			},
			(error) => {
				console.log(error);
			}
		)
	}

	onFormSubmit(value){
		console.log(value);
		this.dialogRef.close(value);
	}

	onClose() {
		this.dialogRef.close();
	}


}
