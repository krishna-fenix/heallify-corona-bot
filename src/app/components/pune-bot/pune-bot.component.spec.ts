import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuneBotComponent } from './pune-bot.component';

describe('PuneBotComponent', () => {
  let component: PuneBotComponent;
  let fixture: ComponentFixture<PuneBotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuneBotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuneBotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
