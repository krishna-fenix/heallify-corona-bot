import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QaBoxComponent } from './qa-box.component';

describe('QaBoxComponent', () => {
  let component: QaBoxComponent;
  let fixture: ComponentFixture<QaBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QaBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QaBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
