import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
	selector: 'app-qa-box',
	templateUrl: './qa-box.component.html',
	styleUrls: ['./qa-box.component.css']
})
export class QaBoxComponent implements OnInit {

	qaData:string = '';
	constructor(
		private dialogRef:MatDialogRef<QaBoxComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit(): void {
		this.qaData = this.data;
	}

	onClose() {
		this.dialogRef.close();
	}


}
