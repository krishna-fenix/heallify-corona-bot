import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReloadAssessmentComponent } from './reload-assessment.component';

describe('ReloadAssessmentComponent', () => {
  let component: ReloadAssessmentComponent;
  let fixture: ComponentFixture<ReloadAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReloadAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReloadAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
