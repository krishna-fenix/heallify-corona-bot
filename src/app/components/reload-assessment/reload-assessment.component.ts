import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
	selector: 'app-reload-assessment',
	templateUrl: './reload-assessment.component.html',
	styleUrls: ['./reload-assessment.component.css']
})
export class ReloadAssessmentComponent implements OnInit {

	constructor(
		private dialogRef:MatDialogRef<ReloadAssessmentComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit(): void {
	}

	onClose() {
		this.dialogRef.close();
	}

	reload(){
		window.location.reload();
	}

}
