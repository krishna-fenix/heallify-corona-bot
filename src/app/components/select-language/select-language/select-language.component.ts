import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
	selector: 'app-select-language',
	templateUrl: './select-language.component.html',
	styleUrls: ['./select-language.component.css']
})
export class SelectLanguageComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<SelectLanguageComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit(): void {

	}

	selectLanguage(value){
		switch (value){
			case 1:
				this.dialogRef.close('english');
				break;
			case 2:
				this.dialogRef.close('hindi');
				break;
			case 3:
				this.dialogRef.close('marathi');
				break;
		}
	}

	onClose() {
		this.dialogRef.close('english');
	}


}
