import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONFIG } from '../../config';
import { map } from 'rxjs/operators';

@Injectable({
  	providedIn: 'root'
})

export class BotServiceService {

	constructor(
		private http:HttpClient
	) { }

	getJSON(): Observable<any> {
		return this.http.get('../../assets/api-endpoint.json');
	}

	getLanguageData():Observable<any>{
		return this.http.get('../../assets/multilingual.json');
	}

	getFirstResponse(session_id, text, contextData) {
		console.log('sessionId' + session_id + 'text' + text);
		const d = { session_id, text, contextData};
		console.log('fsd' + JSON.stringify(d));
		const options = { headers: { 'Content-Type': 'application/json' } };
		return this.http.post(CONFIG.API_URL + 'fulfillment', d, options);
	}
	 
	getResponse(session_id, text) {
		console.log('sessionId' + session_id + 'text' + text);
		const d = { session_id, text};
		console.log('fsd' + JSON.stringify(d));
		const options = { headers: { 'Content-Type': 'application/json' } };
		return this.http.post(CONFIG.API_URL + 'fulfillment', d, options);
	}

	getCurentLocation(lat, long):Observable<any>{
		return this.http.get(CONFIG.MAP_URL + '?latlng='+lat+','+long+'&key='+CONFIG.MAP_API_KEY);
	}

	submitRiskAssessmentForm(url, method, data):Observable<any>{
		const options = { headers: { 'Content-Type': 'application/json' } };
		if(method == "POST")
			return this.http.post(url, data, options);
		else
			return this.http.get(url);
	}

	isAssessmentHelpful(data):Observable<any>{
		const options = { headers: { 'Content-Type': 'application/json' } };
		return this.http.post(CONFIG.API_URL + 'helpful', data, options);
	}

	generateTokenForUniversal():Observable<any>{
		const options = {headers: {
			"Accept": "application/json",
			"api-token": CONFIG.UNIVERSAL_API_TOKEN,
			"user-email": "krishna@fenixwork.com"
		}};
		return this.http.get(CONFIG.UNIVERSAL_API_URL + 'getaccesstoken', options)
	}

	getCountries():Observable<any>{
		const options = {headers: {
			"Accept": "application/json",
			"Authorization":  "Bearer " + CONFIG.UNIVERSAL_AUTH_TOKEN
		}};
		return this.http.get(CONFIG.UNIVERSAL_API_URL + 'countries/', options);
	}

	getStates(country_name):Observable<any>{
		const options = {headers: {
			"Accept": "application/json",
			"Authorization":  "Bearer " + CONFIG.UNIVERSAL_AUTH_TOKEN
		}};
		return this.http.get(CONFIG.UNIVERSAL_API_URL + 'states/' + country_name, options);
	}

	getCities(state_name):Observable<any>{
		const options = {headers: {
			"Accept": "application/json",
			"Authorization":  "Bearer " + CONFIG.UNIVERSAL_AUTH_TOKEN
		}};
		return this.http.get(CONFIG.UNIVERSAL_API_URL + 'cities/' + state_name, options);
	}

}
